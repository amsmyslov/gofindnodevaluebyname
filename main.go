package main

import (
	"encoding/json"
	"fmt"
	"log"
	"reflect"
	"strconv"
)
func logIfError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func FindNodeValueByName(jsonObject interface{}, nodeName string, findNodeName string, findedValue *string) {

	if *findedValue!="not_found" {
		return
	}

	typeOfRootValue:=reflect.TypeOf(jsonObject).String()

	switch typeOfRootValue {
		case "float64":
			jsonObject:=jsonObject.(float64)

			if nodeName==findNodeName {

				if jsonObject == float64(int64(jsonObject)) {
					*findedValue=strconv.FormatFloat(jsonObject,'f',0, 64)
					return

				} else {
					*findedValue=strconv.FormatFloat(jsonObject,'f',6, 64)
					return
				}
			}

		case "string":
			jsonObject:=jsonObject.(string)

			if nodeName==findNodeName {
				*findedValue=jsonObject
			}

		case "map[string]interface {}":
			jsonObject:=jsonObject.(map[string]interface {})

			for key, value := range jsonObject {
				FindNodeValueByName(value,key,findNodeName,findedValue)
			}

		case "[]interface {}":
			jsonObject:=jsonObject.([]interface {})

			for _, value := range jsonObject {
				FindNodeValueByName(value,"",findNodeName,findedValue)
			}

		case "[]uint8":
			jsonObject:=jsonObject.([]byte)

			var root map[string]interface{}
			err := json.Unmarshal(jsonObject, &root)
			logIfError(err)

			for key, value := range root {
				FindNodeValueByName(value,key,findNodeName,findedValue)
			}
	}
}


func main(){
	jsonString :=
		  `{
			"id": 1.4,
			"name": "Mr. Boss",
			"department": "dep",
			"designation": "Director",
			"add":{
				"cit": "Mumbai",
				"stat": "Mahar"
			},
			"address": [
			{
				"city": "Mumbai",
				"state": "Maharashtra",
				"country": "India"
			},
			{	
				"city": "Mumbai",
				"state": "Maharashtra",
				"country": "India"
			}]
		}`


	findedValue:="not_found"
	FindNodeValueByName([]byte(jsonString),"","id",&findedValue)
	fmt.Print(findedValue,"\n\n")
}
